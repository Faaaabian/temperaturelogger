import os #pip install -r requirements.txt
import time
from datetime import datetime
import glob
import sys
import json
from time import strftime
from influxdb import InfluxDBClient

debug = False
values = [0, 0, 0, 0, 0]
python_dictionary = {'measurement': 1}
values_dictionary = {}
os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')
sensors = (os.popen('ls /sys/bus/w1/devices').read()).split('\n')
sensors.pop()
sensors.pop()

# influx
host = "localhost"
port = "8086"
user = "rpi"
password = "rpi"
dbname = "pufferspeicher"

client = InfluxDBClient(host, port, user, password, dbname)


def readSensors(debug):
    if len(sensors) > 0:
        if debug:
            print("found %s sensors" % (len(sensors)))

        for x in range(len(sensors)):
            if debug:
                print("Reading sensor %s (%s)..." % (x+1, sensors[x]))
            values[x] = tempRead(sensors[x])
            if debug:
                print("     ...successful")
        if debug:
            print("read all sensors")
    elif debug:
        print("no sensors found!")


def tempRead(sensor):
    t = open("/sys/bus/w1/devices/%s/w1_slave" % (sensor), 'r')
    lines = t.readlines()
    t.close()

    temp_output = lines[1].find('t=')
    if temp_output != -1:
        temp_string = lines[1].strip()[temp_output+2:]
        temp_c = float(temp_string)/1000.0
    return round(temp_c, 1)

def create_and_send_json_dictionary(dictionary_in):
    now = datetime.now()
    iso = now.strftime("%Y-%m-%dT%H:%M:%SZ")
    local_dict = [
        {
            "measurement": "rpi-ds18b20",
            "tags": {
                "user": "rpi"
            },
            "time": iso,
            "fields": dictionary_in
        }
    ]
    if debug:
        print("Dictionary sent to database:\n%s"%(local_dict))
        print(iso)

    send_to_db(local_dict)


def append_to_values_dictionary(x):
    values_dictionary.update({"%s" % (sensors[x]): values[x]})


def send_to_db(data):
    client.write_points(data)

try:
    if debug:
        print("\n\n\n")
    readSensors(debug)
    print("")
    for x in range(len(sensors)):
        if debug:
            print("from sensor %s: %s" % (sensors[x], values[x]))
        append_to_values_dictionary(x)

    create_and_send_json_dictionary(values_dictionary)
    if debug:
        print("should have sent")
        print("Script ran successful")
except KeyboardInterrupt:
    print("interrupted")
    pass
