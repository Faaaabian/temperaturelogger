# TemperatureLogger

## What is that project about?
Basically I took a few waterproof DS18B20 temperature sensors and connected them to my RaspberryPi. Same one logs the temperature in an influxdb database and uses Grafana to visualize them on a local webpage. I've built this for a watertank at my uncles house, but you can use it anywhere with an unlimited amount of sensors.

## How does that look?
My version looks like this: *picture atm not available*
but you can modify Grafana as you want it to look like.

## How can I build my own monitoring pi?
Just take a RaspberryPi (i recommend using either the latest version or the RaspberryPi 3B+, because that was the one I used.) and attach all sensors as shown here: *picture atm not available* 
Next thing to do is to just follow the steps below (explanation why this is important included).
- create a new linux user [because we don't want crontab to execute with root permissions]
- log into the users shell [needed for following steps]
- clone the git repository in there (just open up a terminal and type `git clone https://gitlab.com/Faaaabian/temperaturelogger.git`) [we do need the script for logging though]
- execute `pip install -r requirements.txt` in the directory [so you have all needed libraries installed]
- set up your influxdb and change my preferences to yours [for this project, a database is required]
- set up Grafana (photo of my setup below) [yeaaah visualisation]
- create a new Dashboard in Grafana and add a graph to it (config/query also below)
- create a crontab, which pulls the repository (and opens up a the local website if you wish so ) at reboot and executes main.py every 10 minutes [for automating the reading]
    
### NOTE
The script can only push data to the database if your preferences are right and you have everything installed properly. Also Grafana can only read data from your database if you choose the right port (rather `IP-adress:3000` than `localhost:3000`) and **if you have data stored in the database**! If there is no data, there will no graphs be available or shown!

## Tips
- Chromium AND Grafana in kiosk mode for the optimal screen fill
- also create a view user (mine is with pw 'view') for your raspberrypi to log in


## Other

If you have any questions or problems you don't know how to deal with feel free to report an issue in gitlab or write me an E-mail (aynril.play@gmail.com) [i will more likely answer if you write me an E-mail]